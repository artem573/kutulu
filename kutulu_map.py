import sys
import math


# Survive the wrath of Kutulu
# Coded fearlessly by JohnnyYuge & nmahoude (ok we might have been a bit scared by the old god...but don't say anything)

width = int(input())
height = int(input())

temp = []
for i in range(height):
    line = input()
    temp.append(line)

field = []
for i in range(width):
    field.append([])
    for j in range(height):
        field[i].append(temp[j][i])
    
N_nodes = (height)*(width)
nodes = []
for i in range(width):
    for j in range(height):
        nodes.append(Pos(i,j))

dist_longest = N_nodes
dist = []
for i in range(N_nodes):
    dist.append([])
    for j in range(N_nodes):
        dist[i].append(dist_longest)

for i in range(N_nodes):


while(True):
for i in range(width):
    for j in range(height):
        

        dist[i + j*height][i + j*height] = 0
        if(field[i][j] != '#' and field[i+1][j] != '#'):
            dist[i + j*height][i+1 + j*height] = 1
            dist[i+1 + j*height][i + j*height] = 1
        if(field[i][j] != '#' and field[i][j+1] != '#'):
            dist[i + j*height][i + (j+1)*height] = 1
            dist[i + (j+1)*height][i + j*height] = 1


print(str(N_nodes) + " " + str(N_nodes_lite), file=sys.stderr)
for k in range(N_nodes_lite):
    for i in range(N_nodes_lite):
        for j in range(N_nodes_lite):
            temp = dist[i][k] + dist[k][j]
            if(dist[i][j] > temp): dist[i][j] = temp

        
# sanity_loss_lonely: how much sanity you lose every turn when alone, always 3 until wood 1
# sanity_loss_group: how much sanity you lose every turn when near another player, always 1 until wood 1
# wanderer_spawn_time: how many turns the wanderer take to spawn, always 3 until wood 1
# wanderer_life_time: how many turns the wanderer is on map after spawning, always 40 until wood 1

sanity_loss_lonely, sanity_loss_group, wanderer_spawn_time, wanderer_life_time = [int(i) for i in input().split()]

# game loop
it = 0
while True:
    it += 1
    wait = True

    pos_wanderer = []

    entity_count = int(input())  # the first given entity corresponds to your explorer
    for i in range(entity_count):
        entity_type, id, x, y, param_0, param_1, param_2 = input().split()
        id = int(id)
        x = int(x)
        y = int(y)
        param_0 = int(param_0)
        param_1 = int(param_1)
        param_2 = int(param_2)



    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    # MOVE <x> <y> | WAIT
    print("WAIT")
    
