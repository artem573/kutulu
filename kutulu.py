import sys
import math

class Pos:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def right(self):
        if(self.x + 1 < width): return Pos(self.x + 1, self.y)
    
    def left(self):
        if(self.x - 1 >= 0): return Pos(self.x - 1, self.y)

    def bottom(self):
        if(self.y + 1 < height): return Pos(self.x, self.y + 1)

    def top(self):
        if(self.y - 1 >= 0): return Pos(self.x, self.y - 1)

    def neighbours(self):
        temp = []
        if(self.right()): temp.append(self.right())
        if(self.left()): temp.append(self.left())
        if(self.top()): temp.append(self.top())
        if(self.bottom()): temp.append(self.bottom())
        return temp


def global_index(pos):
    return pos.x + pos.y*width

def check_node(pos, field, pos_wanderer):
    if(field[pos.x][pos.y] == "#"): return False
    for i in range(len(pos_wanderer)):
        if(pos_wanderer[i].x == pos.x and pos_wanderer[i].y == pos.y): return False
    return True



# Survive the wrath of Kutulu
# Coded fearlessly by JohnnyYuge & nmahoude (ok we might have been a bit scared by the old god...but don't say anything)

width = int(input())
height = int(input())

N_nodes = height*width
dist_longest = N_nodes

temp = []
for i in range(height):
    line = input()
    temp.append(line)

field = []
for i in range(width):
    field.append([])
    for j in range(height):
        field[i].append(temp[j][i])

pos_node = []
for j in range(height):
    for i in range(width):
        pos_node.append(Pos(i,j))

dist = []
for i in range(N_nodes):
    dist.append([])
    for j in range(N_nodes):
        dist[i].append(dist_longest)

for i in range(N_nodes):
    if(field[pos_node[i].x][pos_node[i].y] != "#"):
        dist[i][i] = 0
        step = 0
        current_list = [i]
        while current_list:
            step += 1

            temp_list = []
            for j in range(len(current_list)):
                j_index = current_list[j]
                nbr_list = pos_node[j_index].neighbours()
                for j_nbr in range(len(nbr_list)):
                    pos_nbr = nbr_list[j_nbr]
                    j_nbr_index = global_index(pos_nbr)
                    if((field[pos_nbr.x][pos_nbr.y] != "#") and (dist[i][j_nbr_index] == dist_longest)):
                        dist[i][j_nbr_index] = step
                        found_j_nbr_dubl = False
                        for k in range(len(temp_list)):
                            if(temp_list[k] == j_nbr_index):
                                found_j_nbr_dubl = True
                                break
                        if(not found_j_nbr_dubl): temp_list.append(j_nbr_index)

            if(not temp_list): break
            else: current_list = temp_list
                
# sanity_loss_lonely: how much sanity you lose every turn when alone, always 3 until wood 1
# sanity_loss_group: how much sanity you lose every turn when near another player, always 1 until wood 1
# wanderer_spawn_time: how many turns the wanderer take to spawn, always 3 until wood 1
# wanderer_life_time: how many turns the wanderer is on map after spawning, always 40 until wood 1

sanity_loss_lonely, sanity_loss_group, wanderer_spawn_time, wanderer_life_time = [int(i) for i in input().split()]


it = 0
light_left = 0
plans_left = 0
yell_left = 1

# game loop
while True:
    it += 1

    pos_wanderer = []
    pos_wanderer_hunt_own = []
    wanderer_target_id = []
    pos_explorer = []

    entity_count = int(input())  # the first given entity corresponds to your explorer
    for i in range(entity_count):
        entity_type, id, x, y, param_0, param_1, param_2 = input().split()
        id = int(id)
        x = int(x)
        y = int(y)
        param_0 = int(param_0)
        param_1 = int(param_1)
        param_2 = int(param_2)

        pos = Pos(x, y)
        

        if(i == 0):
            id_own = id
            pos_own = pos
            plans_left = param_1
            light_left = param_2
        if(entity_type == "WANDERER"):
            pos_wanderer.append(pos)
            wanderer_target_id.append(param_2)
            if(param_2 == id_own):
                pos_wanderer_hunt_own.append(pos)
        if(entity_type == "SLASHER"):
            pos_wanderer.append(pos)
        if(entity_type == "EXPLORER"):
            pos_explorer.append(pos)


    action = "WAIT"
    
    # Calc avaliable moves
    avail_moves = []
    if(pos_own.x > 0 and pos_own.x < width and pos_own.y > 0 and pos_own.y < height):
        nbr_list = pos_own.neighbours()
        for i_nbr in range(len(nbr_list)):
            pos = nbr_list[i_nbr]
            if(check_node(pos, field, pos_wanderer)): avail_moves.append(pos)

    # Calc distance to the closest wanderer
    dist_to_closest_wand = dist_longest
    dist_to_closest_wand_hunted_own = dist_longest
    for i in range(len(pos_wanderer)):
        distance = dist[global_index(pos_wanderer[i])][global_index(pos_own)]
        if(dist_to_closest_wand > distance):
            dist_to_closest_wand = distance
            pos_closest_wanderer = pos_wanderer[i]   
            if(wanderer_target_id == id_own):
                dist_to_closest_wand_hunted_own = distance
                    


    # Recalc available moves to go further away from the closest wanderer
    if(pos_wanderer):
        avail_moves_w = []
        for i in range(len(avail_moves)):
            distance = dist[global_index(pos_closest_wanderer)][global_index(avail_moves[i])]
            if(dist_to_closest_wand < distance):
                avail_moves_w.append(avail_moves[i])

        avail_moves = avail_moves_w

    # Recalc available moves to go further away from the wanderers hunting own explorer
    if(pos_wanderer_hunt_own):
        avail_moves_w_ho = []
        for i in range(len(avail_moves)):
            good_move = True
            for j in range(len(pos_wanderer_hunt_own)):
                distance0 = dist[global_index(pos_wanderer_hunt_own[j])][global_index(pos_own)]
                distance  = dist[global_index(pos_wanderer_hunt_own[j])][global_index(avail_moves[i])]
                if(distance0 > distance and distance < dist_to_closest_wand): good_move = False

            if(good_move): avail_moves_w_ho.append(avail_moves[i])

        avail_moves = avail_moves_w_ho
    
    for i in range(len(avail_moves)):
         print("move " + str(avail_moves[i].x) + " " + str(avail_moves[i].y), file=sys.stderr)

    # Calc distance to the closest explorer
    dist_to_closest_explorer = dist_longest
    pos_closest_explorer = pos_own
    for i in range(len(pos_explorer)):
        distance = dist[global_index(pos_explorer[i])][global_index(pos_own)]
        if(dist_to_closest_explorer > distance):
            dist_to_closest_explorer = distance
            pos_closest_explorer = pos_explorer[i]

    # Calc available moves to go near to the closest explorer
    if(pos_explorer):
        avail_moves_ex = []
        for i in range(len(avail_moves)):
            distance = dist[global_index(pos_closest_explorer)][global_index(avail_moves[i])]
            if(dist_to_closest_explorer > distance):
                avail_moves_ex.append(avail_moves[i])

        if(avail_moves_ex): avail_moves = avail_moves_ex

         
    if(avail_moves):
        best_move = avail_moves[0]
        action = "MOVE"

    if((dist_to_closest_wand >= 5) and (plans_left > 0)): action = "PLAN"
    if((dist_to_closest_wand_hunted_own <= 4) and (dist_to_closest_explorer <= 4) and (light_left > 0)): action = "LIGHT"
    if((dist_to_closest_wand <= 3) and (dist_to_closest_explorer <= 1) and (yell_left > 0)):
        yell_left -= 1
        action = "YELL"
                
    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    # MOVE <x> <y> | WAIT
    if(action == "WAIT"): print("WAIT")
    if(action == "YELL"): print("YELL")
    if(action == "PLAN"): print("PLAN")
    if(action == "LIGHT"): print("LIGHT")
    if(action == "MOVE"): print("MOVE " + str(best_move.x) + " " + str(best_move.y))
